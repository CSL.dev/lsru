# lsru

## Setup
### Arch installation
```bash
git clone https://codeberg.org/CSL.dev/lsru.git && cd lsru && makepkg -si
```
### Nix

Using nix3 with flakes, you can run without cloning:
```bash
nix run git+https://codeberg.org/CSL.dev/lsru
```

You can also consume the overlay and add it to your global packages or
home-manager environment.
